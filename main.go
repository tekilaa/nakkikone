package main

import (
	"crypto/rand"
	"fmt"
	"math/big"
)

func main() {
	v, e := rand.Int(rand.Reader, big.NewInt(8))
	if e != nil {
		fmt.Print(e)
	}
	fmt.Print(v, " ")

	i, _ := new(big.Int).SetString(fmt.Sprintf("%v", v), 10)

	num := i.Uint64()
	switch num {
	case 0:
		fmt.Println("Jimi")
	case 1:
		fmt.Println("Riikka")
	case 2:
		fmt.Println("Ville")
	case 3:
		fmt.Println("Amanda")
	case 4:
		fmt.Println("Jimi")
	case 5:
		fmt.Println("Joel")
	case 6:
		fmt.Println("Vara")
	case 7:
		fmt.Println("Heidi")
	}
}
